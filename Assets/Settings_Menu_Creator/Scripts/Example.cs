﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    [SerializeField]
    MenuCreator myMenu;


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            myMenu.ShowMenu();
            Debug.Log("Esc pressed...");
        }
    }
}
