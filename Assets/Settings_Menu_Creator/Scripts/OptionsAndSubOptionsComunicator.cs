﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsAndSubOptionsComunicator : MonoBehaviour
{
    [SerializeField]
    OptionsCreator myOptions;
    [SerializeField]
    SubOptionsCreator mySubOptions;

    public void ActivateAllSuboptionsOfAnOption(List<GameObject> listOptions, string sName){
        mySubOptions.ActivateAllSuboptionsOfAnOption(listOptions, sName);
    }

    public GameObject GetObjectFromListWithIndex(string sValueInDictionary){
        return myOptions.GetObjectFromListWithIndex(sValueInDictionary);
    }

    public int GetListCount(){
        return myOptions.GetListCount(); 
    }

    public GameObject GetOptionPrefab(){
        return(myOptions.GetOptionPrefab());
    }

    public GameObject GetObjectFromListWithIndex(int iIndex){
        return myOptions.GetObjectFromListWithIndex(iIndex);
    }

    public string GetBackButtonText(){
        return myOptions.GetBackButtonText();
    }

}
